App.factory('', function ($http) {
    var url = 'posturl';
    var postHeader = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    this.functionName = function () {
        var data = {}
        data.action = ""
        return $http.post(url, $.param(data), postHeader);
    }
    return this
});